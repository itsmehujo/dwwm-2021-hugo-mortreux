# Exercices de sélection sur la base employees. 

Le schéma décrivant la base se trouve ici : https://dev.mysql.com/doc/employee/en/sakila-structure.html

Vous pouvez accéder à la base à l'adresse suivante : 
```sh
PASSWORD= pipou100f00rm47ion 
psql --host=asciiparait.fr --port 8131 -U courssqldebutant --dbname employes
```
```sh
psql --host=asciiparait.fr --port 8130 -U courssqldebutant --dbname tasks
```

## Requêtes d'interrogation simples. 
Effectuez les requêtes suivantes : 

 1. Afficher le numéro, le nom, le prénom, le genre et la date de naissance de chaque employé. ( emp_no, last_name, first_name,  gender, birth_date )
``` sql
    SELECT emp_no, last_name, first_name, gender, birth_date 
    FROM employees;
```

 2. Trouver tous les employés dont le prénom est 'Troy'. ( emp_no, first_name, last_name, gender )
``` sql
    SELECT emp_no, first_name, last_name, gender 
    FROM employees 
    WHERE first_name = 'Troy';
```

 3. Trouver tous les employés de sexe féminin ( * ).
 ``` sql
    SELECT *
    FROM employees
    WHERE gender = 'F';
 ```
 4. Trouver tous les employés de sexe masculin nés après le 31 janvier 1965 ( * ) ?
 ``` sql
    SELECT * FROM employees
    WHERE gender = 'M'
    AND birth_date > '1965-01-31';
```

 5. Lister uniquement les titres des employés, sans que les valeurs apparaissent plusieurs fois. (title) ?
``` sql
    SELECT DISTINCT title
    FROM titles;
```

 6. Combien y a t'il de département ? ( nombreDep ) ?
 ``` sql
    SELECT COUNT(dept_no) AS "nombreDep"
    FROM departments;
 ```

 7. Qui a eu le salaire maximum de tous les temps, et quel est le montant de ce salaire ? (emp_no, maxSalary) ?
``` sql
    SELECT emp_no, salary
    FROM salaries
    ORDER BY salary DESC
    LIMIT 1;
```

 8. Quel est salaire moyen de l'employé numéro 287323 toute période confondue ?  (emp_no, salaireMoy) ?
``` sql
    SELECT AVG(salary) AS "salaireMoy"
    FROM salaries 
    WHERE emp_no = 287323;
```

 9. Qui sont les employés dont le prénom fini par 'ard' (*) ?
``` sql
    SELECT * 
    FROM employees 
    WHERE first_name ILIKE '%ard';
```

 10. Combien de personnes dont le prénom est 'Richard' sont des femmes ?
``` sql
    SELECT COUNT(first_name) 
    FROM employees
    WHERE first_name = 'Richard'
    AND gender = 'F';
```

 11. Combien y a t'il de titre différents d'employés (nombreTitre) ?
``` sql
    SELECT COUNT(DISTINCT title) AS "nombreTitre"  
    FROM titles;
```

 12. Dans combien de département différents a travaillé l'employé numéro 287323 (nombreDep) ? 
``` sql
    SELECT COUNT(dept_no) AS nombreDep
    FROM dept_emp
    WHERE emp_no = 287323;
```

 13. Quels sont les employés qui ont été embauchés en janvier 2000. (*) ? Les résultats doivent être ordonnés par date d'embauche chronologique
``` sql
    SELECT * FROM employees
    WHERE "hire_date"::text  LIKE '2000-01-%';
    ORDER BY hire_date ASC;
```

 14. Quelle est la somme cumulée des salaires de toute la société (sommeSalaireTotale) ?
``` sql
    SELECT SUM(salary) AS "sommeSalaireTotale" FROM salaries;
```

## Requêtes avec jointures :
 15. Quel était le titre de Danny Rando le 12 janvier 1990 ? (emp_no, first_name, last_name, title) ?
``` sql
    SELECT employees.emp_no, employees.first_name, employees.last_name, titles.title FROM employees
    JOIN titles ON employees.emp_no = titles.emp_no
    WHERE employees.first_name = 'Danny'
    AND employees.last_name = 'Rando'
    AND '1900-01-12' BETWEEN titles.from_date AND titles.to_date;
```

 16. Combien d'employés travaillaient dans le département 'Sales' le 1er Janvier 2000 (nbEmp) ? 
``` sql
    SELECT COUNT(emp_no) AS "nbEmp"  FROM dept_emp
    WHERE dept_no = 'd007'
    AND from_date < '2000-01-01'
    AND to_date > '2000-01-01'
```

 18. Quelle est la somme cumulée des salaires de tous les employés dont le prénom est Richard (emp_no, first_name, last_name, sommeSalaire) ?
``` sql
    SELECT employees.emp_no, employees.first_name, employees.last_name, SUM(salary) AS "SommeSalaire" FROM employees
    JOIN salaries ON employees.emp_no = salaries.emp_no
    WHERE employees.first_name = 'Richard'
    GROUP BY employees.emp_no, employees.first_name, employees.last_name;
```

## Agrégation
 19. Indiquer pour chaque prénom 'Richard', 'Leandro', 'Lena', le nombre de chaque genre (first_name, gender, nombre). Les résultats seront ordonnés par prénom décroissant et genre. 
``` sql
    SELECT DISTINCT first_name, gender, COUNT(gender) AS "nombre" FROM employees
    WHERE first_name = 'Richard'
    OR first_name = 'Leandro'
    OR first_name = 'Lena'
    GROUP BY first_name, gender
    ORDER BY first_name DESC;
```

 20. Quels sont les noms de familles qui apparaissent plus de 200 fois (last_name, nombre) ? Les résultats seront triés par leur nombre croissant et le nom de famille.
``` sql
    SELECT last_name, COUNT(*)
    FROM employees
    GROUP BY last_name
    HAVING COUNT(*) > 200
```

 21. Qui sont les employés dont le prénom est Richard qui ont gagné en somme cumulée plus de 1 000 000 (emp_no, first_name, last_name, hire_date, sommeSalaire) ?
``` sql
    SELECT employees.emp_no, employees.first_name, employees.last_name, employees.hire_date, SUM(salary) AS "sommeSalaire" FROM employees
    JOIN salaries ON employees.emp_no = salaries.emp_no
    WHERE employees.first_name = 'Richard'
    GROUP BY employees.emp_no, employees.first_name, employees.last_name, employees.hire_date
    HAVING SUM(salary) > 1000000;
```

 22. Quel est le numéro, nom, prénom  de l'employé qui a eu le salaire maximum de tous les temps, et quel est le montant de ce salaire ? (emp_no, first_name, last_name, title, maxSalary) 
``` sql
    SELECT employees.emp_no, employees.first_name, employees.last_name, titles.title, MAX(salaries.salary) AS "maxSalary"
    FROM employees
    JOIN salaries ON employees.emp_no = salaries.emp_no 
    JOIN titles ON employees.emp_no = titles.emp_no
    GROUP BY employees.emp_no, employees.first_name, employees.last_name, titles.title
    ORDER BY MAX(salaries.salary) DESC
    LIMIT 1;
```

bonus. Qui est le manager de Martine Hambrick actuellement et quel est son titre (emp_no, first_name, last_name, title)
``` sql
    SELECT employees.emp_no, employees.first_name, employees.last_name, titles.title FROM employees
JOIN dept_emp ON dept_emp.emp_no = employees.emp_no
JOIN titles ON titles.emp_no = employees.emp_no
WHERE (employees.first_name = 'Martine'
    AND employees.last_name = 'Hambrick'
    AND titles.to_date < '2021-06-11')
OR (employees.emp_no = (SELECT dept_manager.emp_no FROM dept_manager
    WHERE dept_manager.dept_no = (SELECT dept_emp.dept_no FROM dept_emp
        WHERE dept_emp.emp_no = (SELECT employees.emp_no FROM employees
            WHERE employees.first_name = 'Martine'
            AND employees.last_name = 'Hambrick')
        )
    AND dept_manager.to_date < '2021-06-11'
    ))
AND titles.to_date < '2021-06-11';

```



## La suite : 
 23. Quel est actuellement le salaire moyen de chaque titre  (title, salaireMoyen) ? Classé par salaireMoyen croissant
``` sql
    SELECT DISTINCT title, AVG(salary) AS "salaireMoyen" FROM titles
    JOIN salaries ON salaries.emp_no = titles.emp_no
    GROUP BY title
    ORDER BY "salaireMoyen" ASC;
```

 24. Combien de manager différents ont eu les différents départements (dept_no, dept_name, nbManagers), Classé par nom de département
``` sql
    SELECT DISTINCT departments.dept_no, departments.dept_name, COUNT(DISTINCT dept_manager.emp_no) AS "nbManagers" FROM departments
    JOIN dept_manager ON dept_manager.dept_no = departments.dept_no
    GROUP BY departments.dept_no, departments.dept_name
    ORDER BY departments.dept_name ASC;
```

 25. Quel est le département de la société qui a le salaire moyen le plus élevé (dept_no, dept_name, salaireMoyen) 
``` sql
    SELECT departments.dept_no, departments.dept_name, AVG(salaries.salary) AS "salaireMoyen" FROM departments
    JOIN dept_emp ON dept_emp.dept_no = departments.dept_no
    JOIN salaries ON dept_emp.emp_no = salaries.emp_no
    GROUP BY departments.dept_no, departments.dept_name
    ORDER BY "salaireMoyen" DESC
    LIMIT 1;
```

 26. Quels sont les employés qui ont eu le titre de 'Senior Staff' sans avoir le titre de 'Staff' ( emp_no , birth_date , first_name , last_name , gender , hire_date )
``` sql
    SELECT employees.emp_no, birth_date, first_name, last_name, gender, hire_date, titles.title FROM employees
    JOIN titles ON employees.emp_no = titles.emp_no
    WHERE titles.title = 'Senior Staff'
    AND titles.title != 'Staff';
```

 27. Indiquer le titre et le salaire de chaque employé lors de leur embauche (emp_no, first_name, last_name, title, salary)
``` sql
    SELECT employees.emp_no, employees.first_name, employees.last_name, titles.title, salaries.salary FROM employees
    JOIN titles ON titles.emp_no = employees.emp_no
    JOIN salaries ON salaries.emp_no = employees.emp_no
    WHERE employees.hire_date = salaries.from_date
    AND employees.hire_date = titles.from_date;

```

 28. Quels sont les employés dont le salaire a baissé (emp_no, first_name, last_name)
``` sql

SELECT DISTINCT employees.emp_no, employees.first_name, employees.last_name FROM employees
JOIN salaries ON salaries.emp_no = employees.emp_no
JOIN egalite ON egalite.emp_no = employees.emp_no
JOIN inegalite ON inegalite.emp_no = employees.emp_no
WHERE egalite.salary > inegalite.salary
LIMIT 20;


CREATE VIEW egalite AS
SELECT salaries.emp_no, salaries.salary, salaries.from_date, salaries.to_date, employees.hire_date FROM salaries
INNER JOIN employees ON employees.emp_no = salaries.emp_no
WHERE salaries.from_date = employees.hire_date;

CREATE VIEW inegalite AS
SELECT salaries.emp_no, salaries.salary, salaries.from_date, salaries.to_date, employees.hire_date FROM salaries
INNER JOIN employees ON employees.emp_no = salaries.emp_no
WHERE salaries.from_date != employees.hire_date;

10335

2 check

2CHECK

SELECT * FROM employees
JOIN salaries ON employees.emp_no = salaries.emp_no
WHERE employees.emp_no = 10335
ORDER BY from_date;